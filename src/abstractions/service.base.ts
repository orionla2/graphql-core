export default abstract class BaseService {
  protected model: any;
  protected core: Map<string, any>;
  protected logger: any;
  constructor(core) {
    this.core = core;
    const modelName = this.constructor.name.replace('Service', 'Model');
    this.model = this.core.get(modelName);
    this.logger = core.get("logger");
  }

  public async getOne(payload) {
    const { id } = payload;
    const response = await this.model.findOne({ id });
    if (!response) throw new Error('wrong id.');
    return this.model.defaultDto(response);
  }

  public async getMany(payload) {
    let { skip, limit, order } = payload;
    const where = {};
    if (skip === undefined) skip = 0;
    if (limit === undefined) limit = 10;
    if (order === undefined) order = '-id';
    const response = await this.model.find(where, skip, limit, order);
    if (!response) throw new Error('wrong id.');
    return response.map(this.model.defaultDto.bind(this.model));
  }

  public async create(payload) {
    return this.model.create(payload);
  }

  public async update(id, entity) {
    await this.model.update(id, entity);
    const response = await this.model.findOne({ id });
    if (!response) throw new Error('wrong id.');
    return this.model.defaultDto(response);
  }

  public async delete(id) {
    const response = await this.model.destroy(id);
    if (!response) throw new Error('wrong id.');
    return response;
  }

  public async count(payload) {
    const where = {};
    for (const key in payload) {
      if (key != 'skip' && key != 'limit' && key != 'order') where[key] = payload[key];
    }
    const response = await this.model.count(where);
    if (typeof response !== 'number') throw new Error('wrong params.');
    return response;
  }

  protected timeLogger(title, start, end) {
    this.logger.info(`[${title}] ${end.diff(start)}ms`)
  }
}
