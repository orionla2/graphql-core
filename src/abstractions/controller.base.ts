export default abstract class BaseController {
  protected service: any;
  protected logger: any;
  protected core: Map<string, any>;
  constructor(core) {
    this.core = core;
    const serviceName = this.constructor.name.replace('Controller', 'Service');
    this.service = this.core.get(serviceName);
    this.logger = core.get("logger");
  }

  public async getOne(ctx, props) {
    return this.service.getOne({ id: props.id });
  }

  public async getMany(ctx, props) {
    return this.service.getMany(props.filter);
  }

  public async count(ctx, props) {
    return this.service.count(props.filter);
  }

  public async create(ctx, props) {
    return this.service.create(props.entity);
  }

  public async update(ctx, props) {
    return this.service.update(props.id, props.entity);
  }

  public async delete(ctx, props) {
    return this.service.delete(props.id);
  }

  protected timeLogger(title, start, end) {
    this.logger.info(`[${title}] ${end.diff(start)}ms`)
  }
}
