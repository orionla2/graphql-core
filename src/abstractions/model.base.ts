export default abstract class BaseModel {
  protected core: Map<string, any>;
  protected logger: any;
  public primaryKey: string;
  public driver: string;
  public model: any;

  constructor(core) {
    this.core = core;
    this.logger = core.get("logger");
    const config = this.core.get('config');
    const db = config.db;
    this.primaryKey = db.pk;
    this.driver = db.driver;
  }

  public save() {
    return this.model.save();
  }

  public getModel() {
    return this.model;
  }

  public includeAll() {
    return [{ all: true, nested: true }];
  }

  public includeAllAttributes() {
    return { exclude: [] };
  }

  public findAndCountAll(include: any = {}) {
    return this.model.findAndCountAll({
      include: include.length ? include : this.includeAll(),
      order: ['id'],
    });
  }

  public findAll(include: any = {}) {
    return this.model.findAll({
      include: include.length ? include : this.includeAll(),
    });
  }

  public find(where, offset = 0, limit = 0, order, include: any = {}) {
    const query: { where: any, order?: any, offset?: any, limit?: any, include?: any } = { where };
    if (limit) {
      query.limit = Number(limit);
      query.offset = Number(offset);
    }
    if (order) query.order = this.orderBy(order);
    query.include = include.length ? include : this.includeAll();
    return this.model.findAll(query);
  }

  public findOne(where, include: any = {}) {
    const query: { where: any, order?: any, offset?: any, include?: any } = { where };
    query.include = include.length ? include : this.includeAll();
    return this.model.findOne(query);
  }

  public findOrCreate(query) {
    return this.model.findOrCreate(query);
  }

  public getByPage(limit, offset, include: any = {}) {
    return this.model.findAndCountAll({
      include: include.length ? include : this.includeAll(),
      limit: Number(limit),
      offset: Number(offset),
      order: ['id'],
    });
  }

  public findOneById(id, include: any = {}) {
    return this.model.findOne({
      include: include.length ? include : this.includeAll(),
      where: {
        id,
      },
    });
  }

  public update(id, fields, transaction) {
    const options: any = {
      where: {
        id,
      },
    };
    if (transaction) options.transaction = transaction;
    return this.model.update(fields, options);
  }

  public create(props, transaction) {
    const options: any = {};
    if (transaction) options.transaction = transaction;
    return this.model.create(props, options);
  }

  public bulkCreate(models, options = {}) {
    return this.model.bulkCreate(models, options);
  }

  public count(where = {}) {
    return this.model.count({ where });
  }

  public destroy(id, transaction) {
    try {
      const options: any = {
        where: {
          id,
        },
      };
      if (transaction) options.transaction = transaction;
      return this.model.destroy(options);
    } catch (e) {
      if (e.code === 'ERR_ASSERTION') {
        return null;
      } else {
        throw e;
      }
    }
  }

  public delete(where, transaction) {
    function isEmpty(object) { for (var i in object) { return false; } return true; }
    try {
      if (isEmpty(where)) throw new Error('wrong "where" params for delete request.');
      const options: any = {
        where,
      };
      if (transaction) options.transaction = transaction;
      return this.model.destroy(options);
    } catch (e) {
      if (e.code === 'ERR_ASSERTION') {
        return null;
      } else {
        throw e;
      }
    }
  }

  public startTransaction() {
    return this.model.sequelize.query('START TRANSACTION;');
  }

  public commit() {
    return this.model.sequelize.query('COMMIT;');
  }

  public rollback() {
    return this.model.sequelize.query('ROLLBACK;');
  }

  public associate() {
    this.model.associate = () => { };
  }

  public orderBy(_field) {
    let field = this.camelToUnderscore(_field);
    if (this.driver === 'sequelize') {
      const sequelize = require('sequelize');
      let direction = 'ASC';
      if (field.slice(0, 1) === '-') {
        direction = 'DESC';
        field = field.substring(1);
      }
      field = [sequelize.literal(`${field} ${direction}`)];
    }
    return field;
  }

  private camelToUnderscore(key) {
    var result = key.replace(/([A-Z])/g, " $1");
    return result.split(' ').join('_').toLowerCase();
  }
}
