export default abstract class BaseRouter {
  protected core: Map<string, any>;
  protected controller: any;
  protected logger: any;
  protected prefix: string;
  protected genericType: string;
  constructor(core) {
    this.core = core;
    this.genericType = this.constructor.name.replace('Router', '');
    this.prefix = this.genericType.charAt(0).toLowerCase() + this.genericType.slice(1);
    const controllerName = this.constructor.name.replace('Router', 'Controller');
    this.controller = this.core.get(controllerName);
    this.logger = core.get("logger");
  }

  protected timeLogger(title, start, end) {
    this.logger.info(`[${title}] ${end.diff(start)}ms`)
  }

  public get schema() {
    return [
      `
        type ${this.genericType} {
          id: ID
        }
        input ${this.genericType}Input {
          id: ID
        }
      `
    ];
  } 

  public get queries() {
    return [
      `
        ${this.prefix}(id: ID): ${this.genericType}
        ${this.prefix}Count(filter: ${this.genericType}Input): Int
        ${this.prefix}All(filter: defaultFilter): [${this.genericType}]
      `
    ];
  } 

  public get mutations() {
    return [
      `
        ${this.prefix}Create(entity: ${this.genericType}Input): ${this.genericType}
        ${this.prefix}Update(id: ID, entity: ${this.genericType}Input): ${this.genericType}
        ${this.prefix}Delete(id: ID): Boolean
      `
    ];
  } 

  public get resolvers() {
    return {
      Query: {
        [`${this.prefix}`]: this.controller.getOne.bind(this.controller),
        [`${this.prefix}All`]: this.controller.getMany.bind(this.controller),
        [`${this.prefix}Count`]: this.controller.count.bind(this.controller),
      },
      Mutation: {
        [`${this.prefix}Create`]: this.controller.create.bind(this.controller),
        [`${this.prefix}Update`]: this.controller.update.bind(this.controller),
        [`${this.prefix}Delete`]: this.controller.delete.bind(this.controller),
      }
    };
  }
}
