import * as express from "express";
import * as cors from "cors";
import { merge } from 'lodash';
import { ApolloServer, gql } from 'apollo-server-express';
import { Kind } from 'graphql/language/kinds';
import GraphQLJSON from 'graphql-type-json';
import { makeExecutableSchema } from 'graphql-tools';

import LoggerService from "./shared-services/logger.service";
import ErrorInterceptor from "./shared-services/error.service";
import DBConnection from "./loaders/db.load";

import { config } from "./config";
import ControllersLoader from "./loaders/controllers.load";
import ServicesLoader from "./loaders/services.load";
import ModelsLoader from "./loaders/models.load";
import RoutersLoader from "./loaders/routers.load";

class Server {
  private core: Map<string, any>;

  constructor(connections: object[]) {
    this.core = this.createCore(connections);
    this.initDependencies();
    this.initServer();
  }

  private initDependencies() {
    //
  }

  private createCore(connections: object[]): Map<string, object> {
    const [db] = connections;
    const core = new Map();
    core.set("config", config);
    core.set(`${config.db.dialect}`, db);
    const app = express();
    core.set("app", app);
    app.use(cors());
    app.use(express.json());
    app.use(express.urlencoded({
      extended: true
    }));
    const logger = new LoggerService(core);
    core.set("logger", logger);
    core.set("ErrorInterceptor", ErrorInterceptor);
    new ModelsLoader(core).load();
    new ServicesLoader(core).load();
    new ControllersLoader(core).load();
    this.initApolloServer(core);
    return core;
  }

  private initApolloServer(core) {
    const app = core.get('app');
    const gqlConfigs = new RoutersLoader(core).load();

    const RootQuery = [
      `type Query {
        ${gqlConfigs.queries.join('')}
      }`
    ];
    const Mutation = [
      `type Mutation {
        ${gqlConfigs.mutations.join('')}
      }`
    ];
    
    const SchemaDefinition = [
      `
      input defaultFilter {
        skip: Int
        limit: Int
        order: String
      }
      `
    ];

    const resolvers = merge(
      gqlConfigs.resolvers
    );

    const schema = [...SchemaDefinition, ...Mutation, ...RootQuery, ...gqlConfigs.schemas];
    const config = makeExecutableSchema({
      typeDefs: schema,
      resolvers,
    });


    const server = new ApolloServer({schema: config});
    server.applyMiddleware({ app });
    core.set('Apollo', server);
  }

  public static initConnections(): Promise<any[]> {
    return Promise.all([
      Server.initPostgres(),
    ]);
  }

  public static initPostgres() {
    return new DBConnection(config.db).load();
  }

  private initServer() {
    const config = this.core.get("config");
    const app = this.core.get("app");
    const PORT = config.server.port;

    app.listen(PORT, () => {
      const Apollo = this.core.get('Apollo');
      this.core.get("logger").verbose(`🚀 Server ready at http://localhost:${PORT}${Apollo.graphqlPath}`)
    });
  }

  public static async run() {
    try {
      const connections = await Server.initConnections();
      new Server(connections);
    } catch (err) {
      console.log("[Error]", err);
    }
  }
}

Server.run();
