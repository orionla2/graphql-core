declare global {
  namespace NodeJS {
    interface ProcessEnv {
      NODE_ENV?: string;
      AMQP_CONNECT?: string;
    }
  }
}

export const config = {
  server: {
    port: process.env.PORT || 3000,
  },
  env: process.env.NODE_ENV || 'dev',
  service: process.env.SERVICE_NAME || "ms-skeleton-app",
  db: {
    driver: "sequelize",
    pk: "id",
    dialect: "postgres",
    database: process.env.POSTGRES_DB || "gql-dev",
    postgres: {
      user: process.env.POSTGRES_USER || "postgres",
      password: process.env.POSTGRES_PASSWORD || "postgres",
      database: process.env.POSTGRES_DB || "gql-dev",
      logging: false,
      port: 5432,
      options: {
        host: 'postgres',
        charset: 'utf8',
        collate: 'utf8_general_ci'
      }
    }
  },
  logger: {
    colorize: true,
    console: true,
    level: "verbose",
    prettyPrint: true,
    transports: [],
    loggerName: process.env.LOGER_NAME || "ms-skeleton-default",
  }
};
