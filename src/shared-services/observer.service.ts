import { observableDiff, applyChange, diff } from "deep-diff";

export default class Observer {
  private subscribers: Function[] = [];
  private privateSubject: object = {};
  private publicSubject: object;

  public createSubject(subj = {}): object {
    if (this.publicSubject)
      throw new Error("Rediclaration of subject is imposible.");
    this.publicSubject = subj;
    return this.publicSubject;
  }

  private set subject(subjChanges) {
    if (this.publicSubject !== subjChanges)
      throw new Error("Wrong subject has been provided");
    this.watch();
  }

  public setSubject(value): void {
    this.subject = value;
  }

  public subscribe(subscriber): void {
    this.subscribers.push(subscriber);
  }

  public unsubscribe(subscriber): void {
    const index = this.subscribers.indexOf(subscriber);
    if (index !== -1) this.subscribers.splice(index, 1);
  }

  public getSubject(): object {
    return this.publicSubject;
  }

  private watch(): void {
    observableDiff(this.publicSubject, this.privateSubject, (d) => {
      applyChange(this.privateSubject, this.subject, d);
      this.notify();
    });
  }

  private notify(): void {
    this.subscribers.forEach((subscriber) => {
      // @ts-ignore
      subscriber.call();
    });
  }

  public diff(rhs, lhs) {
    return diff(lhs, rhs);
  }

  public destroy(): void {
    this.subscribers.forEach((subscriber) => {
      this.unsubscribe(subscriber);
    });
  }
}
