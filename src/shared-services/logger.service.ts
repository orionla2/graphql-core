import { Loggly } from "winston-loggly-bulk";
const winston = require("winston");

export default class LoggerService {
  private name: string;
  private core: any;

  constructor(core?: Map<any, any>, name?: string) {
    this.core = core;
    const config = core.get('config');
    const options = config.logger;
    this.name = name || options.loggerName;
    const logger = winston.createLogger(options);

    if (options.console) {
      logger.add(
        new winston.transports.Console({
          format: winston.format.combine(
            winston.format.colorize(),
            winston.format.printf((i) => `${i.level}: ${i.message}`)
          ),
          level: options.level,
        })
      );
    }

    if (options.loggly) {
      logger.add(new Loggly({
        token: options.loggly.token,
        subdomain: options.loggly.subdomain,
        tags: options.loggly.tags,
        json: options.loggly.json,
      }));
    }

    if (options.syslog) {
      logger.add(
        new winston.transports.Syslog({
          host: process.env.SYSLOG_HOST,
        })
      );
    }

    ["verbose", "debug", "info", "warn", "error"].forEach((item) => {
      this[item] = function (msg) {
        const payload = {
          env: config.env,
          service: config.service,
          loggerName: this.name,
          message: {
            level: item,
            data: msg,
          },
        };
        if (options.rabbitmq) this.sendLogMessage(payload);
        logger[item].call(logger, msg);
      };
    });
  }

  private sendLogMessage(msg) {
    const channel = this.core.get('rabbitmq');
    const config = this.core.get('config');
    channel.sendToQueue(
      config.logger.rabbitmq.channel,
      Buffer.from(JSON.stringify(msg))
    );
  }

  static create(name, core) {
    if (!name && typeof name !== 'string') throw new Error('Logger name is required as firs param.');
    const instance: any = new LoggerService(core, name);
    return instance;
  }
}
