export default class ErrorInterceptor {
  public code: number;
  public message: string;
  private logger: Function | null;
  private getError: Function;
  private debug: boolean;
  constructor({ code, error, logger }) {
    this.code = code || 400;
    this.message = error.message;
    this.getError = () => {
      return error.stack;
    };
    this.logger = logger || null;
    this.debug = Boolean(process.env.DEBUG) || false;
    this.log();
  }

  private log() {
    if (this.logger) this.logger(this.getLogJSON());
  }

  private getLogJSON() {
    const result = {
      code: this.code,
      message: this.message,
    };
    if (this.debug) result["stack"] = this.getOriginalError();
    return JSON.stringify(result);
  }

  public getOriginalError() {
    return this.getError();
  }

  public toJSON(): string {
    return this.getLogJSON();
  }
}
