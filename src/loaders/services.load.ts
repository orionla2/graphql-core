import * as path from "path";
import Loader from "./loader";

export default class ServicesLoader extends Loader {
  constructor(core) {
    super(core);
    this.RESOURCES_TYPE = "Services";
    this.INTERNAL_RESOURCES_PATH = path.join(
      __dirname,
      "../modules/**/*.service.*s"
    );
  }
}
