import * as path from "path";
import Loader from "./loader";

export default class ControllersLoader extends Loader {
  constructor(core) {
    super(core);
    this.RESOURCES_TYPE = "Controllers";
    this.INTERNAL_RESOURCES_PATH = path.join(
      __dirname,
      "../modules/**/*.controller.*s"
    );
  }
}
