import * as path from "path";
import Loader from "./loader";

import { mergeWith } from 'lodash';

export default class RoutersLoader extends Loader {
  constructor(core) {
    super(core);
    this.RESOURCES_TYPE = "router";
    this.INTERNAL_RESOURCES_PATH = path.join(
      __dirname,
      "../modules/**/*.router.*s"
    );
  }

  load() {
    const routes = super.load();
    const res = routes.reduce((graphConfig, router) => {
      graphConfig.schemas = graphConfig.schemas.concat(router.schema);
      graphConfig.queries = graphConfig.queries.concat(router.queries);
      graphConfig.mutations = graphConfig.mutations.concat(router.mutations);
      graphConfig.resolvers = mergeWith(graphConfig.resolvers, router.resolvers);
      return graphConfig;
    }, {
      schemas: [],
      queries: [],
      mutations: [],
      resolvers: {}
    });
    return res;
  }

  protected iterate(resourcePath, core) {
    const Resource = require(resourcePath);
    let Class;
    if (Resource.default instanceof Function) {
      Class = Resource.default;
    } else if (Resource instanceof Function) {
      Class = Resource;
    }
    if (!Class) {
      this.logger.error(
        `${this.RESOURCES_TYPE} at path \`${resourcePath}\` has not name.`
      );
      throw new Error(
        `${this.RESOURCES_TYPE} at path \`${resourcePath}\` has not name.`
      );
    }

    return new Class(core);
  }
}