import { Sequelize } from 'sequelize';

export default class DBConnection {
  private db;
  private driver;
  private dialect;
  private connection;
  private dbDialect;

  constructor(config) {
    this.db = config;
    this.driver = this.db.driver;
    this.dialect = this.db.dialect;
    this.dbDialect = this.db[this.dialect];
  }

  public async load() {
    try {
      if (this.driver === 'sequelize') await this.connectSequelDb();
      return this.connection;
    } catch (err) {
      throw new Error('DB Server connection failed');
    }
  }

  private async connectSequelDb() {
    this.connection = new Sequelize(
      this.dbDialect.database,
      this.dbDialect.user,
      this.dbDialect.password,
      {
        define: {
          charset: 'utf8' || this.dbDialect.options.charset,
          collate: 'utf8_general_ci' || this.dbDialect.options.collate,
          timestamps: false,
        },
        dialect: this.dialect,
        host: this.dbDialect.options.host,
        logging: this.dbDialect.logging ? console.log : false,
        operatorsAliases: null,
        pool: {
          acquire: 30000,
          idle: 10000,
          max: 5,
          min: 0,
        },
        port: this.db.port,
        query: { raw: this.dbDialect.raw || false }
      },
    );
    await this.connection.authenticate();
    console.log(`[SEQUELIZE::CONNECTED]`);
  }
}