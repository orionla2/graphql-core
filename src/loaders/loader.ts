import * as glob from "glob";

export default abstract class Loader {
  protected core: Map<string, any>;
  protected INTERNAL_RESOURCES_PATH: string;
  protected RESOURCES_TYPE: string;
  protected logger: any;

  constructor(core) {
    this.core = core;
    this.logger = core.get("logger");
  }

  public load() {
    if (!this.INTERNAL_RESOURCES_PATH) {
      throw new Error("INTERNAL_RESOURCES_PATH should be defined.");
    }
    const internalResources = glob.sync(this.INTERNAL_RESOURCES_PATH);
    this.logger.info(`[Start loading ${this.RESOURCES_TYPE}]`);
    return internalResources.map((resourcePath) =>
      this.iterate(resourcePath, this.core)
    );
  }

  protected iterate(resourcePath, core) {
    let Resource = require(resourcePath);
    let resourceObj;
    let name;

    if (Resource instanceof Function) {
      name = Resource.name;
      resourceObj = new Resource(core);
    } else if (Resource.default instanceof Function) {
      Resource = Resource.default;
      name = Resource.name;
      resourceObj = new Resource(core);
    } else {
      name = Resource.name;
      resourceObj = Resource;
    }
    if (!name) {
      this.logger.error(
        `${this.RESOURCES_TYPE} at path \`${resourcePath}\` has not name.`
      );
      throw new Error(
        `${this.RESOURCES_TYPE} at path \`${resourcePath}\` has not name.`
      );
    } else {
      this.core.set(name, resourceObj);
      this.logger.info(`\`${name}\` has initialized.`);
    }
    return name;
  }
}
