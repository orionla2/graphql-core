import * as path from "path";
import Loader from "./loader";

export default class ModelsLoader extends Loader {
  constructor(core) {
    super(core);
    this.RESOURCES_TYPE = "Models";
    this.INTERNAL_RESOURCES_PATH = path.join(
      __dirname,
      "../modules/**/*.model.*s"
    );
  }

  load() {
    const models = super.load();
    //Uncoment in case of Sequelize relational database.
    if (process.env.NODE_ENV !== 'test') {
      models.forEach((name) => {
        const model = this.core.get(name);
        model.associate();
      });
    }
  }
}
