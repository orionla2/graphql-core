import BaseService from "../../abstractions/service.base";
import { Op } from 'sequelize';

export default class UsersService extends BaseService {
  constructor(core) {
    super(core);
  }
}