import BaseController from '../../abstractions/controller.base'

export default class UsersController extends BaseController {
  constructor(core) {
    super(core);
  }

  public async getOne(ctx, props) {
    return this.service.getOne({ id: props.id} );
  }

  public async getMany(ctx, props) {
    return this.service.getMany(props.filter);
  }

  public async create(ctx, props) {
    return this.service.create(props.entity);
  }

  public async update(ctx, props) {
    return this.service.update(props.id, props.entity);
  }

  public async delete(ctx, props) {
    return this.service.delete(props.id);
  }
}