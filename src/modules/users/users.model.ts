import Model from '../../abstractions/model.base';
import { schema as schemaDefinition } from './users.schema';

export default class UsersModel extends Model {
  public db;
  public model;

  constructor(core: any) {
    super(core);
    const config = this.core.get('config');
    const dialect = config.db.dialect;
    this.db = this.core.get(`${dialect}`);
    this.model = this.db.define('users', schemaDefinition, {
      freezeTableName: true,
      paranoid: false,
      timestamps: false,
      underscored: true,
    });
  }

  public associate() {}

  public defaultDto(model: any) {
    return {
      id: model.id,
      email: model.email,
      name: model.name,
      age: model.age,
    };
  }
}
