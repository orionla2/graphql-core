import BaseRouter from '../../abstractions/router.base';

export default class UsersRouter extends BaseRouter {
  constructor(core) {
    super(core);
  }

  get schema() {
    return [
      `
        type ${this.genericType} {
          id: ID!
          name: String
          email: String
          age: Int
        }

        input ${this.genericType}Input {
          name: String
          email: String
          age: Int
        }
      `
    ];
  }
}
