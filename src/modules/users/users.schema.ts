import { INTEGER, STRING, BOOLEAN, DATE } from 'sequelize';
export const schema = {
  id: {
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: INTEGER,
    unique: true,
  },
  email: {
    allowNull: false,
    type: STRING(255),
  },
  name: {
    allowNull: false,
    type: STRING(255),
  },
  age: {
    allowNull: false,
    type: INTEGER,
  },
};