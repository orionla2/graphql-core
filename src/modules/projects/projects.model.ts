import Model from '../../abstractions/model.base';
import { schema as schemaDefinition } from './projects.schema';

export default class ProjectsModel extends Model {
  public db;
  public model;

  constructor(core: any) {
    super(core);
    const config = this.core.get('config');
    const dialect = config.db.dialect;
    this.db = this.core.get(`${dialect}`);
    this.model = this.db.define('projects', schemaDefinition, {
      freezeTableName: true,
      paranoid: false,
      timestamps: false,
      underscored: true,
    });
  }

  public associate() {
    const UsersModel = this.core.get('UsersModel');
    this.model.belongsTo(UsersModel.model, { foreignKey: 'user', as: 'userEntity' });
  }

  public defaultDto(model: any) {
    const UsersModel = this.core.get('UsersModel');
    return {
      id: model.id,
      name: model.name,
      status: model.status,
      user: UsersModel.defaultDto(model.userEntity),
    };
  }
}
