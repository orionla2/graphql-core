import { INTEGER, STRING, ENUM } from 'sequelize';
export const schema = {
  id: {
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
    type: INTEGER,
    unique: true,
  },
  user: {
    allowNull: false,
    type: INTEGER,
    field: 'user_id',
    references: {
      model: 'users',
      key: 'id',
    }
  },
  name: {
    allowNull: false,
    type: STRING(255),
  },
  status: {
    allowNull: false,
    type: ENUM,
    values: ['OPEN', 'PENDING', 'PROCESSING', 'DECLINED', 'CLOSED'],
  },
};