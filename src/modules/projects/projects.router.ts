import BaseRouter from '../../abstractions/router.base';

export default class ProjectsRouter extends BaseRouter {
  constructor(core) {
    super(core);
  }

  get schema() {
    return [
      `
        enum PROJECT_STATUS {
          OPEN
          PENDING
          PROCESSING
          DECLINED
          CLOSED
        }
        type ${this.genericType} {
          id: ID!
          name: String
          user: Users
          status: PROJECT_STATUS
        }

        input ${this.genericType}Input {
          name: String
          user: Int
          status: PROJECT_STATUS
        }
      `
    ];
  }
}
