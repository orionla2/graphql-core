import BaseService from "../../abstractions/service.base";
import { Op } from 'sequelize';

export default class ProjectsService extends BaseService {
  constructor(core) {
    super(core);
  }
}