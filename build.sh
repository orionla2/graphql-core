#!/bin/bash
imagePrfix="gql"
imageTitle="$imagePrfix-$1"
imageTag=${2-latest}
filePath=${3-.}
imageName="$imageTitle:$imageTag"
available=([0]=gateway)

echo -e "\e[32mCheck ability to process: \e[33m$1"
if printf "%b\n" "${available[@]}" | grep -P "^$1$"; 
  then
  echo -e "\e[32mbuild service \e[33m$1"
  echo -e "\e[32mimage -> \e[33m$imageName \e[0m"
  echo -e "\e[32mStart building process \e[0m"
  docker build -t $imageName -f dockerfile.$1.dev $filePath
  echo -e "\e[32mBuild process ended \e[0m"

  else
  echo -e "\e[31mInvalid service name: \e[33m$1 \e[0m"
fi
